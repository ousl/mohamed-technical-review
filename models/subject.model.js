const mongoose = require("mongoose");

const subjectSchema = new mongoose.Schema({
  subjectName: {
    type: String,
    required: true,
  },
  subjectCode: {
    type: String,
    required: true,
    unique: true,
  },
  description: String,
  studentMarks: [
    {
      student: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Student",
      },
      marks: Number, // You can use a specific data type for marks, such as Number
    },
  ],
  // Add more fields as needed
});

const Subject = mongoose.model("Subject", subjectSchema);

module.exports = Subject;
