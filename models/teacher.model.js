const mongoose = require("mongoose");

const teacherSchema = new mongoose.Schema({
  firstName: String,
  lastName: String,
  email: { type: String, unique: true },
  password: String,
});

const Teacher = mongoose.model("Teacher", teacherSchema);

module.exports = Teacher;
