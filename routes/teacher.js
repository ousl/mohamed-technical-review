const express = require("express");
const router = express.Router();
const Teacher = require("../models/teacher.model"); // Import Teacher Model
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

// Teacher login route
router.post("/login", async (req, res) => {
  const { email, password } = req.body;

  try {
    const teacher = await Teacher.findOne({ email });

    if (!teacher) {
      return res.status(404).json({ message: "Teacher not found" });
    }

    // Implement password validation logic using bcrypt
    const passwordMatch = await bcrypt.compare(password, teacher.password);

    if (passwordMatch) {
      // Generate a JWT token
      const token = jwt.sign({ userId: teacher._id }, "your-secret-key", {
        expiresIn: "1h", // Token expiration time (adjust as needed)
      });

      return res.status(200).json({
        message: "Successfully logged in",
        url: "/teacher/dashboard",
        token,
      });
    } else {
      return res.status(401).json({ message: "Invalid Credentials" });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal server error", message: error });
  }
});

// Teacher registration route
router.post("/register", async (req, res) => {
  const { firstName, lastName, email, password } = req.body;

  try {
    // Check if the email is already registered
    const existingTeacher = await Teacher.findOne({ email });

    if (existingTeacher) {
      return res.status(409).json({ message: "Teacher exists" });
    }

    // Hash the password before storing it
    const saltRounds = 10;
    const hashedPassword = await bcrypt.hash(password, saltRounds);

    // Create a new teacher in the database
    const newTeacher = new Teacher({
      firstName,
      lastName,
      email,
      password: hashedPassword,
    });

    await newTeacher.save();
    return res.status(201).json({ message: "Teacher created" });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal server error", message: error });
  }
});

module.exports = router;
