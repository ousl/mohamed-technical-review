const express = require("express");
const router = express.Router();
const Subject = require("../models/subject.model");
const Student = require("../models/student.model");

// Update subject marks for a student
router.put("/update/:studentEmail/:subjectCode", async (req, res) => {
  const { studentEmail, subjectCode } = req.params;
  const { marksObtained } = req.body;

  try {
    // Check if the student and subject exist
    const student = await Student.findOne({ email: studentEmail });
    if (!student) {
      return res.status(404).json({ message: "Student not found" });
    }

    const subject = await Subject.findOne({ subjectCode: subjectCode });
    if (!subject) {
      return res.status(404).json({ message: "Subject not found" });
    }

    // Find the student's entry in the subjectMarks array
    const studentMarksEntry = subject.studentMarks.find(
      (entry) => entry.student._id.toString() === student._id.toString()
    );

    // Update marks if the entry exists, otherwise, create a new entry
    if (studentMarksEntry) {
      studentMarksEntry.marks = marksObtained;
    } else {
      subject.studentMarks.push({
        student: student._id,
        marks: marksObtained,
        email: student.email,
      });
    }

    await subject.save();

    res.status(200).json({ message: "Subject marks updated", subject });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal server error", message: error });
  }
});

module.exports = router;
