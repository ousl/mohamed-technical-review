const express = require("express");
const router = express.Router();
const Subject = require("../models/subject.model");

// Create a new subject
router.post("/create", async (req, res) => {
  try {
    const { subjectName, subjectCode, description } = req.body;

    // Create a new subject
    const newSubject = new Subject({
      subjectName,
      subjectCode,
      description,
    });

    await newSubject.save();
    res.status(201).json({ message: "Subject created", subject: newSubject });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal server error", message: error });
  }
});

// Get all subjects
router.get("/", async (req, res) => {
  try {
    const subjects = await Subject.find();
    res.status(200).json(subjects);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal server error" });
  }
});

// Get a specific subject by subjectCode
router.get("/:subjectCode", async (req, res) => {
  const subjectCode = req.params.subjectCode;
  try {
    const subject = await Subject.findOne({ subjectCode });
    if (!subject) {
      return res.status(404).json({ message: "Subject not found" });
    }
    res.status(200).json(subject);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal server error" });
  }
});

// Update a subject's information
router.put("/:subjectCode", async (req, res) => {
  const subjectCode = req.params.subjectCode;
  const { subjectName, description } = req.body;

  try {
    const updatedSubject = await Subject.findOneAndUpdate(
      { subjectCode },
      { subjectName, description },
      { new: true }
    );

    if (!updatedSubject) {
      return res.status(404).json({ message: "Subject not found" });
    }

    res
      .status(200)
      .json({ message: "Subject updated", subject: updatedSubject });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal server error", message: error });
  }
});

// Delete a subject by subjectCode
router.delete("/:subjectCode", async (req, res) => {
  const subjectCode = req.params.subjectCode;
  try {
    const deletedSubject = await Subject.findOneAndRemove({ subjectCode });
    if (!deletedSubject) {
      return res.status(404).json({ message: "Subject not found" });
    }
    res
      .status(200)
      .json({ message: "Subject deleted", subject: deletedSubject });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
