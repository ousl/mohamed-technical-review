const express = require("express");
const router = express.Router();
const Student = require("../models/student.model"); // import Student Model
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

// Student login route
router.post("/login", async (req, res) => {
  const { email, password } = req.body;

  try {
    const student = await Student.findOne({ email });

    if (!student) {
      return res.status(404).json({ message: "user not found" });
    }

    // Implement password validation logic using bcrypt
    const passwordMatch = await bcrypt.compare(password, student.password);

    if (passwordMatch) {
      // Generate a JWT token
      const token = jwt.sign({ userId: student._id }, "your-secret-key", {
        expiresIn: "1h", // Token expiration time (adjust as needed)
      });
      // Set a session variable to track user authentication
      //   req.session.studentId = Student._id;
      return res.status(200).json({
        message: "success fully logged in",
        url: "/student/dashboard",
        token,
      });
    } else {
      return res.status(401).json({ message: "Invalid Credentials" });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal server error", message: error });
  }
});

// Student registration route
router.post("/register", async (req, res) => {
  const { firstName, lastName, email, password, age } = req.body;

  try {
    // Check if the email is already registered
    const existingStudent = await Student.findOne({ email });

    if (existingStudent) {
      return res.status(409).json({ message: "Student exsist" });
    }

    // Hash the password before storing it
    const saltRounds = 10;
    const hashedPassword = await bcrypt.hash(password, saltRounds);

    // Create a new student in the database
    const newStudent = new Student({
      firstName,
      lastName,
      email,
      age,
      password: hashedPassword,
    });

    await newStudent.save();
    return res.status(201).json({ message: "Student created" });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Internal server error", message: error });
  }
});

module.exports = router;
