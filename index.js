const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const session = require("express-session");
const jwt = require("jsonwebtoken");
const studentRoutes = require("./routes/student");
const teacherRoutes = require("./routes/teacher");
const subjectRoutes = require("./routes/subject");
const subjectMarksRoutes = require("./routes/subjectMarks");

// Verify JWT token
function verifyToken(req, res, next) {
  const token = req.headers.authorization; // Assuming the token is passed in the 'Authorization' header

  if (!token) {
    return res.status(401).json({ message: "Unauthorized" });
  }

  jwt.verify(token, "your-secret-key", (err, decoded) => {
    if (err) {
      return res.status(401).json({ message: "Invalid token" });
    }
    req.userId = decoded.userId;
    console.log("decoded.userId", decoded.userId);
    next();
  });
}

// Connect to MongoDB
mongoose.connect("mongodb://localhost/student_registration_db", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

// Create and configure your Express app
const app = express();

// Middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static("public"));

app.use("/student", studentRoutes);
app.use("/teacher", teacherRoutes);
app.use("/subjects", verifyToken, subjectRoutes);
app.use("/subject-marks", verifyToken, subjectMarksRoutes);

// Handle errors
app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).send("Something broke!");
});

// Start the server
const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
